const https = require("https");
const http = require("http");
const csv = require('csvtojson');

CvmJs = {
    getHttps: function(options) {
        return https.get(options.url, res => {
            res.setEncoding(options.encoding || "utf8");
            let body = "";
            res.on("data", data => {
                body += data;
            });
            res.on("end", () => {
                options.onSuccess && options.onSuccess(body);
            });
        });
    },
    getHttp: function(options) {
        return http.get(options.url, res => {
            res.setEncoding(options.encoding || "utf8");
            let body = "";
            res.on("data", data => {
                body += data;
            });
            res.on("end", () => {
                options.onSuccess && options.onSuccess(body);
            });
        });
    },
    CiaAberta: {
        getDadosCadastrais: function(onRow, onDone) {
            return CvmJs.getHttp({
                url: "http://dados.cvm.gov.br/dados/CIA_ABERTA/CAD/DADOS/inf_cadastral_cia_aberta.csv",
                encoding: "latin1",
                onSuccess: function(data) {
                    csv({ delimiter: ";" })
                        .fromString(data)
                        .on('json', onRow)
                        .on('done', onDone);
                }
            });
        }
    }
};


module.exports = CvmJs;